import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class BluetoothWidget extends StatefulWidget {
  @override
  _BluetoothWidgetState createState() => _BluetoothWidgetState();
}

class _BluetoothWidgetState extends State<BluetoothWidget> {
  FlutterBlue _flutterBlue = FlutterBlue.instance;
  Map<DeviceIdentifier, ScanResult> _scanResults = new Map();
  bool _displayItems = false;
  bool _isLoading = false;
  bool _isConnecting = false;
  bool _writePressed = false;
  bool _newNotification = false;
  bool _subscribe = false;
  String _isConnected = "";
  String _notificationMessage = "";
  List<BluetoothService> _services = [];
  late BluetoothCharacteristic _mainCharacteristic;

  void _updateBluetoothList() async {
    setState(() {
      _isLoading = true;
    });
    _scanResults = new Map();

    await _flutterBlue.startScan(timeout: Duration(seconds: 4));

    _flutterBlue.scanResults.listen((results) {
      for (ScanResult r in results) {
        print('${r.device.name} found! rssi: ${r.rssi}');
        _scanResults[r.device.id] = r;
      }
      setState(() {
        _displayItems = !_displayItems;
        _isLoading = false;
      });
    });

    _flutterBlue.stopScan();
  }

  void _connectToDevice(BluetoothDevice device) async {
    if (_isConnected == "") {
      setState(() {
        _isLoading = true;
        _isConnecting = true;
      });

      await device.connect();
      print("finished connection");
      print(device);
      _services = await device.discoverServices();
      _services.forEach((service) {
        if (service.uuid.toString() == "0000ffe0-0000-1000-8000-00805f9b34fb") {
          List<BluetoothCharacteristic> blueChar = service.characteristics;
          blueChar.forEach((c) async {
            if (c.uuid.toString() == "0000ffe1-0000-1000-8000-00805f9b34fb") {
              setState(() {
                _mainCharacteristic = c;
              });
              return;
            }
          });
        }
      });

      setState(() {
        _isLoading = false;
        _isConnecting = false;
        _isConnected = device.name;
      });
    }
  }

  List<Widget> showWriteDisplay() {
    List<Widget> widgetToReturn = [];
    if (_writePressed == true) {
      widgetToReturn.add(TextField(
        onSubmitted: (value) async {
          await _mainCharacteristic.write(utf8.encode(value + '\n'));
          setState(() {
            _writePressed = false;
          });
        },
      ));
    }
    return widgetToReturn;
  }

  Widget _bluetoothListGenerator(BuildContext context, int idx) {
    var keys = _scanResults.keys.toList();

    if (_scanResults[keys[idx]]!.device.name != "") {
      final device = _scanResults[keys[idx]]!.device.name;
      return new Container(
          margin: const EdgeInsets.all(10),
          child: Column(
            children: [
              ElevatedButton(
                child: Row(
                  children: [Text(device.toString())],
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
                onPressed: () async {
                  _connectToDevice(_scanResults[keys[idx]]!.device);
                  // final mtu = await _scanResults[keys[idx]]!.device.mtu.first;
                  // await _scanResults[keys[idx]]!.device.requestMtu(512);
                },
              ),
              if (_isConnected == _scanResults[keys[idx]]!.device.name) ...{
                new Container(
                    margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: ElevatedButton(
                      child: Row(
                        children: [
                          Text(_subscribe ? "Unsubscribe" : "Subscribe")
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                      ),
                      onPressed: () async {
                        await _mainCharacteristic
                            .setNotifyValue(!_mainCharacteristic.isNotifying);
                        setState(() {
                          _subscribe = !_subscribe;
                        });
                        _mainCharacteristic.value.listen((event) async {
                          // TODO: implement a logic to check before utf8 conversion for \0
                          if (utf8.decode(event) == "1234") {
                            await _mainCharacteristic
                                .write(utf8.encode("A0000000041010" + '\n'));
                          } else {
                            _notificationMessage =
                                utf8.decode(event).toString();
                            if (_notificationMessage != "") {
                              setState(() {
                                _newNotification = true;
                              });
                            }
                          }
                        });
                      },
                    )),
                new Container(
                    margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: ElevatedButton(
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Text("Write"),
                            ],
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                      ),
                      onPressed: () {
                        print("Write");
                        setState(() {
                          _writePressed = true;
                        });
                      },
                    )),
                ...showWriteDisplay(),
              }
            ],
          ));
    } else
      return Row();
  }

  List<Widget> bluetoothSearchDisplay() {
    List<Widget> widgetToReturn = [];
    if (_isLoading) {
      if (_isConnecting) {
        widgetToReturn.add(AlertDialog(
            title: const Text('Connecting to your device'),
            content: SingleChildScrollView(
                child: ListBody(
              children: const <Widget>[
                Text("We're connecting you to your device"),
              ],
            ))));
      } else {
        widgetToReturn.add(AlertDialog(
            title: const Text('Looking for devices'),
            content: SingleChildScrollView(
                child: ListBody(
              children: const <Widget>[
                Text("We're looking for bluetooth devices"),
              ],
            ))));
      }
      widgetToReturn.add(Center(
        child: CircularProgressIndicator(),
      ));
    } else if (_newNotification) {
      widgetToReturn.add(AlertDialog(
        title: const Text('New notification'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("Message :"),
              Text(_notificationMessage),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Ok'),
            onPressed: () {
              setState(() {
                _newNotification = false;
              });
            },
          ),
        ],
      ));
    } else {
      widgetToReturn.add(Expanded(
          child: Center(
        child: ListView.builder(
            itemCount: _scanResults.length,
            itemBuilder: _bluetoothListGenerator),
      )));
    }
    return widgetToReturn;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text("List of bluetooth devices"),
          ...bluetoothSearchDisplay(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _updateBluetoothList,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
